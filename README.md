# Example Course Lecture Notes

These are lecture notes taken during Prof. Dr. Example Teacher's course Example Course (summer semester 2022).

## Reading

If you just want to read these lecture notes, download the file "Example Course - lecture notes.pdf".

## Requirements for Compilation

To compile these notes my package [hrftex](https://github.com/r0uv3n/hrftex) is required.
